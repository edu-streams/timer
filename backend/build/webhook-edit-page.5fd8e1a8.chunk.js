"use strict";(self.webpackChunktimer=self.webpackChunktimer||[]).push([[5162],{558:(N,b,t)=>{t.d(b,{Z:()=>De});var e=t(67294),o=t(64289),D=t(185),C=t(88767),p=t(16550),M=t(48474),ve=t(45697),d=t.n(ve),be=t(90321),ne=t(85018),Ce=t(67109),z=t(53979),m=t(11047),se=t(29728),Z=t(49066),W=t(41580),X=t(11276),O=t(74571),B=t(16364),T=t(41054),j=t(86896),re=t(19270),x=t(75515),le=t(46449),Me=t(36213),Pe=t(92155),xe=t(11700),G=t.n(xe);const a=({disabledEvents:n,name:s,events:r,inputValue:l,handleChange:u,handleChangeAll:f})=>{const c=r.filter(g=>!n.includes(g)),h=l.length===c.length,i=l.length>0,A=({target:{name:g}})=>{f({target:{name:g,value:!h}})};return e.createElement("tr",null,e.createElement("td",null,e.createElement(Me.X,{indeterminate:i&&!h,"aria-label":"Select all entries",name:s,onChange:A,value:h},G()(s))),r.map(g=>e.createElement("td",{key:g},e.createElement(Pe.C,{disabled:n.includes(g),"aria-label":g,name:g,value:l.includes(g),onValueChange:v=>u({target:{name:g,value:v}})}))))};a.defaultProps={disabledEvents:[],events:[],inputValue:[],handleChange(){},handleChangeAll(){}},a.propTypes={disabledEvents:d().array,events:d().array,inputValue:d().array,handleChange:d().func,handleChangeAll:d().func,name:d().string.isRequired};const P=a,J=n=>n.reduce((s,r)=>{const l=r.split(".")[0];return s[l]||(s[l]=[]),s[l].push(r),s},{}),_=le.ZP.table`
  td {
    height: ${52/16}rem;
    width: 10%;
    vertical-align: middle;
    text-align: center;
  }

  tbody tr:nth-child(odd) {
    background: ${({theme:n})=>n.colors.neutral100};
  }

  tbody tr td:first-child {
    padding-left: ${({theme:n})=>n.spaces[7]};
  }
`,R={headers:{default:[{id:"Settings.webhooks.events.create",defaultMessage:"Create"},{id:"Settings.webhooks.events.update",defaultMessage:"Update"},{id:"app.utils.delete",defaultMessage:"Delete"}],draftAndPublish:[{id:"Settings.webhooks.events.create",defaultMessage:"Create"},{id:"Settings.webhooks.events.update",defaultMessage:"Update"},{id:"app.utils.delete",defaultMessage:"Delete"},{id:"app.utils.publish",defaultMessage:"Publish"},{id:"app.utils.unpublish",defaultMessage:"Unpublish"}]},events:{default:{entry:["entry.create","entry.update","entry.delete"],media:["media.create","media.update","media.delete"]},draftAndPublish:{entry:["entry.create","entry.update","entry.delete","entry.publish","entry.unpublish"],media:["media.create","media.update","media.delete"]}}},w=({isDraftAndPublish:n})=>{const s=n?R.headers.draftAndPublish:R.headers.default,r=n?R.events.draftAndPublish:R.events.default,{formatMessage:l}=(0,j.Z)(),{values:u,handleChange:f}=(0,T.u6)(),c="events",h=u.events,i=[],A=J(h),g=({target:{name:E,value:ae}})=>{let k=new Set(h);ae?k.add(E):k.delete(E),f({target:{name:c,value:Array.from(k)}})},v=({target:{name:E,value:ae}})=>{let k=new Set(h);ae?r[E].forEach(H=>{i.includes(H)||k.add(H)}):r[E].forEach(H=>k.delete(H)),f({target:{name:c,value:Array.from(k)}})};return e.createElement(m.k,{direction:"column",alignItems:"stretch",gap:1},e.createElement(re.Q,null,l({id:"Settings.webhooks.form.events",defaultMessage:"Events"})),e.createElement(_,null,e.createElement("thead",null,e.createElement("tr",null,e.createElement("td",null),s.map(E=>E==="app.utils.publish"||E==="app.utils.unpublish"?e.createElement("td",{key:E.id,title:l({id:"Settings.webhooks.event.publish-tooltip",defaultMessage:"This event only exists for content with draft & publish enabled"})},e.createElement(x.Z,{variant:"sigma",textColor:"neutral600"},l(E))):e.createElement("td",{key:E.id},e.createElement(x.Z,{variant:"sigma",textColor:"neutral600"},l(E)))))),e.createElement("tbody",null,Object.keys(r).map(E=>e.createElement(P,{disabledEvents:i,key:E,name:E,events:r[E],inputValue:A[E],handleChange:g,handleChangeAll:v})))))};w.propTypes={isDraftAndPublish:d().bool.isRequired};const ye=w;var Ie=t(96315),Oe=t(39785),Te=t(29178),Re=t(90608);const ie=["A-IM","Accept","Accept-Charset","Accept-Encoding","Accept-Language","Accept-Datetime","Access-Control-Request-Method","Access-Control-Request-Headers","Authorization","Cache-Control","Connection","Content-Length","Content-Type","Cookie","Date","Expect","Forwarded","From","Host","If-Match","If-Modified-Since","If-None-Match","If-Range","If-Unmodified-Since","Max-Forwards","Origin","Pragma","Proxy-Authorization","Range","Referer","TE","User-Agent","Upgrade","Via","Warning"],Y=({name:n,onChange:s,value:r,...l})=>{const[u,f]=(0,e.useState)(r?[...ie,r]:ie),c=i=>{s({target:{name:n,value:i}})},h=i=>{f(A=>[...A,i]),s({target:{name:n,value:i}})};return e.createElement(Te.XU,{...l,onChange:c,onCreateOption:h,placeholder:"",value:r},u.map(i=>e.createElement(Re.O,{value:i,key:i},i)))};Y.defaultProps={value:void 0},Y.propTypes={name:d().string.isRequired,onChange:d().func.isRequired,value:d().string};const de=Y,ue=()=>{const{formatMessage:n}=(0,j.Z)(),{values:s,errors:r}=(0,T.u6)();return e.createElement(m.k,{direction:"column",alignItems:"stretch",gap:1},e.createElement(re.Q,null,n({id:"Settings.webhooks.form.headers",defaultMessage:"Headers"})),e.createElement(W.x,{padding:8,background:"neutral100",hasRadius:!0},e.createElement(T.F2,{validateOnChange:!1,name:"headers",render:({push:l,remove:u})=>e.createElement(X.r,{gap:4},s.headers?.map((f,c)=>e.createElement(e.Fragment,{key:c},e.createElement(O.P,{col:6},e.createElement(T.gN,{as:de,name:`headers.${c}.key`,"aria-label":`row ${c+1} key`,label:n({id:"Settings.webhooks.key",defaultMessage:"Key"}),error:r.headers?.[c]?.key&&n({id:r.headers[c]?.key,defaultMessage:r.headers[c]?.key})})),e.createElement(O.P,{col:6},e.createElement(m.k,{alignItems:"flex-end"},e.createElement(W.x,{style:{flex:1}},e.createElement(T.gN,{as:B.o,"aria-label":`row ${c+1} value`,label:n({id:"Settings.webhooks.value",defaultMessage:"Value"}),name:`headers.${c}.value`,error:r.headers?.[c]?.value&&n({id:r.headers[c]?.value,defaultMessage:r.headers[c]?.value})})),e.createElement(m.k,{paddingLeft:2,style:{alignSelf:"center"},paddingTop:r.headers?.[c]?.value?0:5},e.createElement(o.fG,{onClick:()=>s.headers.length!==1&&u(c),label:n({id:"Settings.webhooks.headers.remove",defaultMessage:"Remove header row {number}"},{number:c+1})})))))),e.createElement(O.P,{col:12},e.createElement(Oe.A,{type:"button",onClick:()=>{l({key:"",value:""})},startIcon:e.createElement(Ie.Z,null)},n({id:"Settings.webhooks.create.header",defaultMessage:"Create new header"}))))})))};var he=t(86647),q=t(70968);const K=le.ZP.svg(({theme:n,color:s})=>`
  width: ${12/16}rem;
  height: ${12/16}rem;

  path {
    fill: ${n.colors[s]};
  }
`),V=({isPending:n,statusCode:s})=>{const{formatMessage:r}=(0,j.Z)();return n?e.createElement(m.k,{gap:2,alignItems:"center"},e.createElement(K,{as:he.Z}),e.createElement(x.Z,null,r({id:"Settings.webhooks.trigger.pending",defaultMessage:"pending"}))):s>=200&&s<300?e.createElement(m.k,{gap:2,alignItems:"center"},e.createElement(K,{as:ne.Z,color:"success700"}),e.createElement(x.Z,null,r({id:"Settings.webhooks.trigger.success",defaultMessage:"success"}))):s>=300?e.createElement(m.k,{gap:2,alignItems:"center"},e.createElement(K,{as:q.Z,color:"danger700"}),e.createElement(x.Z,null,r({id:"Settings.error",defaultMessage:"error"})," ",s)):null};V.propTypes={isPending:d().bool.isRequired,statusCode:d().number},V.defaultProps={statusCode:void 0};const F=({statusCode:n,message:s})=>{const{formatMessage:r}=(0,j.Z)();return n>=200&&n<300?e.createElement(m.k,{justifyContent:"flex-end"},e.createElement(x.Z,{textColor:"neutral600",ellipsis:!0},r({id:"Settings.webhooks.trigger.success.label",defaultMessage:"Trigger succeeded"}))):n>=300?e.createElement(m.k,{justifyContent:"flex-end"},e.createElement(m.k,{maxWidth:(0,o.Q1)(250),justifyContent:"flex-end",title:s},e.createElement(x.Z,{ellipsis:!0,textColor:"neutral600"},s))):null};F.propTypes={statusCode:d().number,message:d().string},F.defaultProps={statusCode:void 0,message:void 0};const me=({onCancel:n})=>{const{formatMessage:s}=(0,j.Z)();return e.createElement(m.k,{justifyContent:"flex-end"},e.createElement("button",{onClick:n,type:"button"},e.createElement(m.k,{gap:2,alignItems:"center"},e.createElement(x.Z,{textColor:"neutral400"},s({id:"Settings.webhooks.trigger.cancel",defaultMessage:"cancel"})),e.createElement(K,{as:q.Z,color:"neutral400"}))))};me.propTypes={onCancel:d().func.isRequired};const ee=({isPending:n,onCancel:s,response:r})=>{const{statusCode:l,message:u}=r,{formatMessage:f}=(0,j.Z)();return e.createElement(W.x,{background:"neutral0",padding:5,shadow:"filterShadow",hasRadius:!0},e.createElement(X.r,{gap:4,style:{alignItems:"center"}},e.createElement(O.P,{col:3},e.createElement(x.Z,null,f({id:"Settings.webhooks.trigger.test",defaultMessage:"Test-trigger"}))),e.createElement(O.P,{col:3},e.createElement(V,{isPending:n,statusCode:l})),e.createElement(O.P,{col:6},n?e.createElement(me,{onCancel:s}):e.createElement(F,{statusCode:l,message:u}))))};ee.defaultProps={isPending:!1,onCancel(){},response:{}},ee.propTypes={isPending:d().bool,onCancel:d().func,response:d().object};const te=ee;var y=t(87561);const $=/(^$)|(^[A-Za-z][_0-9A-Za-z ]*$)/,ge=/(^$)|((https?:\/\/.*)(d*)\/?(.*))/,Ee=y.Ry().shape({name:y.Z_(o.I0.string).required(o.I0.required).matches($,o.I0.regex),url:y.Z_(o.I0.string).required(o.I0.required).matches(ge,o.I0.regex),headers:y.Vo(n=>{let s=y.IX();if(n.length===1){const{key:r,value:l}=n[0];if(!r&&!l)return s}return s.of(y.Ry().shape({key:y.Z_().required(o.I0.required),value:y.Z_().required(o.I0.required)}))}),events:y.IX()}),Q=({handleSubmit:n,data:s,triggerWebhook:r,isCreating:l,isTriggering:u,triggerResponse:f,isDraftAndPublishEvents:c})=>{const{formatMessage:h}=(0,j.Z)(),[i,A]=(0,e.useState)(!1);return e.createElement(T.J9,{onSubmit:n,initialValues:{name:s?.name||"",url:s?.url||"",headers:Object.keys(s?.headers||[]).length?Object.entries(s.headers).map(([g,v])=>({key:g,value:v})):[{key:"",value:""}],events:s?.events||[]},validationSchema:Ee,validateOnChange:!1,validateOnBlur:!1},({handleSubmit:g,errors:v})=>e.createElement(o.l0,{noValidate:!0},e.createElement(z.T,{primaryAction:e.createElement(m.k,{gap:2},e.createElement(se.z,{onClick:()=>{r(),A(!0)},variant:"tertiary",startIcon:e.createElement(be.Z,null),disabled:l||u,size:"L"},h({id:"Settings.webhooks.trigger",defaultMessage:"Trigger"})),e.createElement(se.z,{startIcon:e.createElement(ne.Z,null),onClick:g,type:"submit",size:"L"},h({id:"global.save",defaultMessage:"Save"}))),title:l?h({id:"Settings.webhooks.create",defaultMessage:"Create a webhook"}):s?.name,navigationAction:e.createElement(o.rU,{startIcon:e.createElement(Ce.Z,null),to:"/settings/webhooks"},h({id:"global.back",defaultMessage:"Back"}))}),e.createElement(Z.D,null,e.createElement(m.k,{direction:"column",alignItems:"stretch",gap:4},i&&e.createElement("div",{className:"trigger-wrapper"},e.createElement(te,{isPending:u,response:f,onCancel:()=>A(!1)})),e.createElement(W.x,{background:"neutral0",padding:8,shadow:"filterShadow",hasRadius:!0},e.createElement(m.k,{direction:"column",alignItems:"stretch",gap:6},e.createElement(X.r,{gap:6},e.createElement(O.P,{col:6},e.createElement(T.gN,{as:B.o,name:"name",error:v.name&&h({id:v.name}),label:h({id:"global.name",defaultMessage:"Name"}),required:!0})),e.createElement(O.P,{col:12},e.createElement(T.gN,{as:B.o,name:"url",error:v.url&&h({id:v.url}),label:h({id:"Settings.roles.form.input.url",defaultMessage:"Url"}),required:!0}))),e.createElement(ue,null),e.createElement(ye,{isDraftAndPublish:c})))))))};Q.propTypes={data:d().object,handleSubmit:d().func.isRequired,triggerWebhook:d().func.isRequired,isCreating:d().bool.isRequired,isDraftAndPublishEvents:d().bool.isRequired,isTriggering:d().bool.isRequired,triggerResponse:d().object},Q.defaultProps={data:void 0,triggerResponse:void 0};const pe=Q,Ae=n=>({...n,headers:S(n.headers)}),S=n=>n.reduce((s,r)=>{const{key:l,value:u}=r;return l!==""?{...s,[l]:u}:s},{}),fe=Ae,De=()=>{const{params:{id:n}}=(0,p.$B)("/settings/webhooks/:id"),{replace:s}=(0,p.k6)(),{lockApp:r,unlockApp:l}=(0,o.o1)(),u=(0,o.lm)(),f=(0,C.useQueryClient)(),{isLoading:c,collectionTypes:h}=(0,M.bP)(),{put:i,get:A,post:g}=(0,o.kY)(),v=n==="create",{isLoading:E,data:ae}=(0,C.useQuery)(["get-webhook",n],async()=>{try{const{data:{data:I}}=await A(`/admin/webhooks/${n}`);return I}catch{return u({type:"warning",message:{id:"notification.error"}}),null}},{enabled:!v}),{isLoading:k,data:H,isIdle:$e,mutate:Se}=(0,C.useMutation)(()=>g(`/admin/webhooks/${n}/trigger`)),Le=()=>Se(null,{onError(){u({type:"warning",message:{id:"notification.error"}})}}),Ze=(0,C.useMutation)(I=>g("/admin/webhooks",I)),We=(0,C.useMutation)(({id:I,body:L})=>i(`/admin/webhooks/${I}`,L)),Be=async I=>{v?(r(),Ze.mutate(fe(I),{onSuccess({data:L}){u({type:"success",message:{id:"Settings.webhooks.created"}}),s(`/settings/webhooks/${L.data.id}`),l()},onError(L){u({type:"warning",message:{id:"notification.error"}}),console.log(L),l()}})):(r(),We.mutate({id:n,body:fe(I)},{onSuccess(){f.invalidateQueries(["get-webhook",n]),u({type:"success",message:{id:"notification.form.success.fields"}}),l()},onError(L){u({type:"warning",message:{id:"notification.error"}}),console.log(L),l()}}))},Ue=e.useMemo(()=>h.some(I=>I.options.draftAndPublish===!0),[h]);return E||c?e.createElement(o.dO,null):e.createElement(D.o,null,e.createElement(o.SL,{name:"Webhooks"}),e.createElement(pe,{handleSubmit:Be,data:ae,triggerWebhook:Le,isCreating:v,isTriggering:k,isTriggerIdle:$e,triggerResponse:H?.data.data,isDraftAndPublishEvents:Ue}))}},3672:(N,b,t)=>{t.r(b),t.d(b,{default:()=>M});var e=t(67294),o=t(64289),D=t(87751),C=t(558);const M=()=>e.createElement(o.O4,{permissions:D.Z.settings.webhooks.create},e.createElement(C.Z,null))},42122:(N,b,t)=>{t.r(b),t.d(b,{default:()=>M});var e=t(67294),o=t(64289),D=t(87751),C=t(558);const M=()=>e.createElement(o.O4,{permissions:D.Z.settings.webhooks.update},e.createElement(C.Z,null))},29178:(N,b,t)=>{t.d(b,{Wx:()=>xe,XU:()=>T,hQ:()=>B});var e=t(85893),o=t(67294),D=t(70968),C=t(12645),p=t(61505),M=t(46449),ve=t(10892),d=t(2504),be=t(75368),ne=t(15585),Ce=t(77197),z=t(41580),m=t(75515),se=t(54574),Z=t(11047),W=t(19270),X=t(63428),O=t(96404);const B=({children:a,clearLabel:P="clear",creatable:U=!1,createMessage:J=te=>`Create "${te}"`,defaultTextValue:_,disabled:R=!1,error:w,hasMoreItems:ye=!1,hint:Ie,id:Oe,label:Te,labelAction:Re,loading:oe=!1,loadingMessage:ie="Loading content...",noOptionsMessage:Y=()=>"No results found",onChange:de,onClear:ce,onCreateOption:ue,onInputChange:he,onLoadMore:q,placeholder:K="Select or enter a value",required:V=!1,startIcon:F,textValue:me,value:ee})=>{const[te,y]=o.useState(!1),[$,ge]=(0,ve.T)({prop:me,defaultProp:_}),[je,Ee]=o.useState(""),Q=o.useRef(null),pe=o.useRef(null),Ae=o.useRef(null),S=(0,d.M)(Oe),fe=i=>{ce&&!R&&(ge(""),Ee(""),ce(i),pe.current.focus())},ke=i=>{y(i)},De=i=>{ge(i)},n=i=>{Ee(i)},s=i=>{he&&he(i)},r=i=>{de&&de(i)},l=`intersection-${CSS.escape(S)}`,u=i=>{q&&ye&&!oe&&q(i)},f=()=>{ue&&$&&ue($)};(0,be.s)(Q,u,{selectorToWatch:`#${l}`,skipWhen:!te});const c=`${S}-hint`,h=`${S}-error`;return(0,e.jsx)(se.g,{hint:Ie,error:w,id:S,required:V,children:(0,e.jsxs)(Z.k,{direction:"column",alignItems:"stretch",gap:1,children:[(0,e.jsx)(W.Q,{action:Re,children:Te}),(0,e.jsxs)(p.hQ.Root,{autocomplete:U?"list":"both",onOpenChange:ke,onTextValueChange:De,textValue:$,allowCustomValue:!0,disabled:R,required:V,value:ee,onValueChange:r,filterValue:je,onFilterValueChange:n,children:[(0,e.jsxs)(re,{$hasError:!!w,children:[(0,e.jsxs)(Z.k,{flex:"1",as:"span",gap:3,children:[F?(0,e.jsx)(z.x,{as:"span","aria-hidden":!0,children:F}):null,(0,e.jsx)(x,{placeholder:K,id:S,"aria-invalid":!!w,"aria-labelledby":`${c} ${h}`,onChange:s,ref:pe})]}),(0,e.jsxs)(Z.k,{as:"span",gap:3,children:[$&&ce?(0,e.jsx)(j,{as:"button",hasRadius:!0,background:"transparent",type:"button",onClick:fe,"aria-disabled":R,"aria-label":P,title:P,ref:Ae,children:(0,e.jsx)(D.Z,{})}):null,(0,e.jsx)(le,{children:(0,e.jsx)(C.Z,{})})]})]}),(0,e.jsx)(p.hQ.Portal,{children:(0,e.jsx)(Me,{sideOffset:4,children:(0,e.jsxs)(Pe,{ref:Q,children:[a,U?(0,e.jsx)(p.hQ.CreateItem,{onPointerUp:f,onClick:f,asChild:!0,children:(0,e.jsx)(G,{children:(0,e.jsx)(m.Z,{children:J($??"")})})}):null,!U&&!oe?(0,e.jsx)(p.hQ.NoValueFound,{asChild:!0,children:(0,e.jsx)(G,{$hasHover:!1,children:(0,e.jsx)(m.Z,{children:Y($??"")})})}):null,oe?(0,e.jsx)(Z.k,{justifyContent:"center",alignItems:"center",paddingTop:2,paddingBottom:2,children:(0,e.jsx)(Ce.a,{small:!0,children:ie})}):null,(0,e.jsx)(z.x,{id:l,width:"100%",height:"1px"})]})})})]}),(0,e.jsx)(X.J,{}),(0,e.jsx)(O.c,{})]})})},T=a=>(0,e.jsx)(B,{...a,creatable:!0}),j=(0,M.ZP)(z.x)`
  border: none;

  svg {
    height: ${11/16}rem;
    width: ${11/16}rem;
  }

  svg path {
    fill: ${({theme:a})=>a.colors.neutral600};
  }
`,re=(0,M.ZP)(p.hQ.Trigger)`
  position: relative;
  border: 1px solid ${({theme:a,$hasError:P})=>P?a.colors.danger600:a.colors.neutral200};
  padding-right: ${({theme:a})=>a.spaces[3]};
  padding-left: ${({theme:a})=>a.spaces[3]};
  border-radius: ${({theme:a})=>a.borderRadius};
  background: ${({theme:a})=>a.colors.neutral0};
  overflow: hidden;
  display: flex;
  align-items: center;
  justify-content: space-between;
  gap: ${({theme:a})=>a.spaces[4]};

  &[data-disabled] {
    color: ${({theme:a})=>a.colors.neutral600};
    background: ${({theme:a})=>a.colors.neutral150};
    cursor: not-allowed;
  }

  /* Required to ensure the below inputFocusStyles are adhered too */
  &:focus-visible {
    outline: none;
  }

  ${({theme:a,$hasError:P})=>(0,ne.k3)()({theme:a,hasError:P})};
`,x=(0,M.ZP)(p.hQ.TextInput)`
  width: 100%;
  font-size: ${14/16}rem;
  color: ${({theme:a})=>a.colors.neutral800};
  min-height: ${40/16}rem;
  border: none;
  background-color: transparent;

  &:focus-visible {
    outline: none;
  }

  &[aria-disabled='true'] {
    cursor: inherit;
  }
`,le=(0,M.ZP)(p.hQ.Icon)`
  & > svg {
    width: ${6/16}rem;

    & > path {
      fill: ${({theme:a})=>a.colors.neutral600};
    }
  }

  &[aria-disabled='true'] {
    cursor: inherit;
  }
`,Me=(0,M.ZP)(p.hQ.Content)`
  background: ${({theme:a})=>a.colors.neutral0};
  box-shadow: ${({theme:a})=>a.shadows.filterShadow};
  border: 1px solid ${({theme:a})=>a.colors.neutral150};
  border-radius: ${({theme:a})=>a.borderRadius};
  width: var(--radix-combobox-trigger-width);
  /* This is from the design-system figma file. */
  max-height: 15rem;
  z-index: ${({theme:a})=>a.zIndices[1]};
`,Pe=(0,M.ZP)(p.hQ.Viewport)`
  padding: ${({theme:a})=>a.spaces[1]};
`,xe=o.forwardRef(({children:a,value:P,disabled:U,textValue:J,..._},R)=>(0,e.jsx)(p.hQ.ComboboxItem,{asChild:!0,value:P,disabled:U,textValue:J,children:(0,e.jsx)(G,{ref:R,..._,children:(0,e.jsx)(p.hQ.ItemText,{asChild:!0,children:(0,e.jsx)(m.Z,{children:a})})})})),G=M.ZP.div`
  width: 100%;
  border: none;
  text-align: left;
  outline-offset: -3px;
  padding: ${({theme:a})=>a.spaces[2]} ${({theme:a})=>a.spaces[4]};
  background-color: ${({theme:a})=>a.colors.neutral0};
  border-radius: ${({theme:a})=>a.borderRadius};
  user-select: none;

  &[data-selected] {
    background-color: ${({theme:a})=>a.colors.primary100};

    ${m.Z} {
      color: ${({theme:a})=>a.colors.primary600};
      font-weight: bold;
    }
  }

  &:hover,
  &[data-highlighted] {
    outline: none;
    background-color: ${({theme:a,$hasHover:P=!0})=>P?a.colors.primary100:a.colors.neutral0};
  }

  &[data-highlighted] {
    ${m.Z} {
      color: ${({theme:a})=>a.colors.primary600};
      font-weight: bold;
    }
  }
`},90608:(N,b,t)=>{t.d(b,{O:()=>o});var e=t(29178);const o=e.Wx},67109:(N,b,t)=>{t.d(b,{Z:()=>D});var e=t(85893);const o=C=>(0,e.jsx)("svg",{xmlns:"http://www.w3.org/2000/svg",width:"1rem",height:"1rem",fill:"none",viewBox:"0 0 24 24",...C,children:(0,e.jsx)("path",{fill:"#212134",d:"M24 13.3a.2.2 0 0 1-.2.2H5.74l8.239 8.239a.2.2 0 0 1 0 .282L12.14 23.86a.2.2 0 0 1-.282 0L.14 12.14a.2.2 0 0 1 0-.282L11.86.14a.2.2 0 0 1 .282 0L13.98 1.98a.2.2 0 0 1 0 .282L5.74 10.5H23.8c.11 0 .2.09.2.2v2.6Z"})}),D=o}}]);
