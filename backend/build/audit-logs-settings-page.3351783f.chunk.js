"use strict";(self.webpackChunktimer=self.webpackChunktimer||[]).push([[2812],{94699:(F,v,t)=>{t.d(v,{Z:()=>M});var e=t(67294),o=t(45697),E=t.n(o),y=t(86896),m=t(41580),f=t(29728),P=t(89597),T=t(64289);const x=({displayedFilters:g})=>{const[n,O]=(0,e.useState)(!1),{formatMessage:Z}=(0,y.Z)(),R=(0,e.useRef)(),A=()=>{O($=>!$)};return e.createElement(e.Fragment,null,e.createElement(m.x,{paddingTop:1,paddingBottom:1},e.createElement(f.z,{variant:"tertiary",ref:R,startIcon:e.createElement(P.Z,null),onClick:A,size:"S"},Z({id:"app.utils.filters",defaultMessage:"Filters"})),n&&e.createElement(T.J5,{displayedFilters:g,isVisible:n,onToggle:A,source:R})),e.createElement(T.W$,{filtersSchema:g}))};x.propTypes={displayedFilters:E().arrayOf(E().shape({name:E().string.isRequired,metadatas:E().shape({label:E().string}),fieldSchema:E().shape({type:E().string})})).isRequired};const M=x},35915:(F,v,t)=>{t.r(v),t.d(v,{default:()=>re});var e=t(67294),o=t(64289),E=t(87751),y=t(86896),m=t(17034),f=t(49066),P=t(41580),T=t(185),x=t(53979),M=t(36989),g=t(45697),n=t.n(g),O=t(15234),Z=t(79031),R=t(37909),A=t(75515),$=t(11047),le=t(12028),G=t(8934),ie=t(23855);const w=()=>{const{formatDate:s}=(0,y.Z)();return c=>{const l=(0,ie.Z)(c),p=s(l,{dateStyle:"long"}),i=s(l,{timeStyle:"medium",hourCycle:"h24"});return`${p}, ${i}`}},k={"entry.create":"Create entry{model, select, undefined {} other { ({model})}}","entry.update":"Update entry{model, select, undefined {} other { ({model})}}","entry.delete":"Delete entry{model, select, undefined {} other { ({model})}}","entry.publish":"Publish entry{model, select, undefined {} other { ({model})}}","entry.unpublish":"Unpublish entry{model, select, undefined {} other { ({model})}}","media.create":"Create media","media.update":"Update media","media.delete":"Delete media","media-folder.create":"Create media folder","media-folder.update":"Update media folder","media-folder.delete":"Delete media folder","user.create":"Create user","user.update":"Update user","user.delete":"Delete user","admin.auth.success":"Admin login","admin.logout":"Admin logout","content-type.create":"Create content type","content-type.update":"Update content type","content-type.delete":"Delete content type","component.create":"Create component","component.update":"Update component","component.delete":"Delete component","role.create":"Create role","role.update":"Update role","role.delete":"Delete role","permission.create":"Create permission","permission.update":"Update permission","permission.delete":"Delete permission"},V=s=>k[s]||s,Q=({headers:s,rows:u,onOpenModal:c})=>{const{formatMessage:l}=(0,y.Z)(),p=w(),i=({type:d,value:r,model:L})=>d==="date"?p(r):d==="action"?l({id:`Settings.permissions.auditLogs.${r}`,defaultMessage:V(r)},{model:L}):r||"-";return e.createElement(O.p,null,u.map(d=>e.createElement(Z.Tr,{key:d.id,...(0,o.X7)({fn:()=>c(d.id)})},s.map(({key:r,name:L,cellFormatter:C})=>e.createElement(R.Td,{key:r},e.createElement(A.Z,{textColor:"neutral800"},i({type:r,value:C?C(d[L]):d[L],model:d.payload?.model})))),e.createElement(R.Td,{...o.UW},e.createElement($.k,{justifyContent:"end"},e.createElement(le.h,{onClick:()=>c(d.id),"aria-label":l({id:"app.component.table.view",defaultMessage:"{target} details"},{target:`${d.action} action`}),noBorder:!0,icon:e.createElement(G.Z,null)}))))))};Q.defaultProps={rows:[]},Q.propTypes={headers:n().array.isRequired,rows:n().array,onOpenModal:n().func.isRequired};const de=Q,N=[{name:"action",key:"action",metadatas:{label:{id:"Settings.permissions.auditLogs.action",defaultMessage:"Action"},sortable:!0}},{name:"date",key:"date",metadatas:{label:{id:"Settings.permissions.auditLogs.date",defaultMessage:"Date"},sortable:!0}},{key:"user",name:"user",metadatas:{label:{id:"Settings.permissions.auditLogs.user",defaultMessage:"User"},sortable:!1},cellFormatter:s=>s?s.displayName:""}],a=({pagination:s})=>e.createElement(P.x,{paddingTop:4},e.createElement($.k,{alignItems:"flex-end",justifyContent:"space-between"},e.createElement(o.v4,null),e.createElement(o.tU,{pagination:s})));a.defaultProps={pagination:{pageCount:0,pageSize:50,total:0}},a.propTypes={pagination:n().shape({page:n().number,pageCount:n().number,pageSize:n().number,total:n().number})};const D=a;var j=t(88767),H=t(42866),z=t(24969),b=t(2407),J=t(59946),ce=t(77197),me=t(11276),ue=t(26614);const q=({actionLabel:s,actionName:u})=>e.createElement($.k,{direction:"column",alignItems:"baseline",gap:1},e.createElement(A.Z,{textColor:"neutral600",variant:"sigma"},s),e.createElement(A.Z,{textColor:"neutral600"},u));q.propTypes={actionLabel:n().string.isRequired,actionName:n().string.isRequired};const W=q,U=({status:s,data:u,formattedDate:c})=>{const{formatMessage:l}=(0,y.Z)();if(s==="loading")return e.createElement($.k,{padding:7,justifyContent:"center",alignItems:"center"},e.createElement(ce.a,null,"Loading content..."));const{action:p,user:i,payload:d}=u;return e.createElement(e.Fragment,null,e.createElement(P.x,{marginBottom:3},e.createElement(A.Z,{variant:"delta",id:"title"},l({id:"Settings.permissions.auditLogs.details",defaultMessage:"Log Details"}))),e.createElement(me.r,{gap:4,gridCols:2,paddingTop:4,paddingBottom:4,paddingLeft:6,paddingRight:6,marginBottom:4,background:"neutral100",hasRadius:!0},e.createElement(W,{actionLabel:l({id:"Settings.permissions.auditLogs.action",defaultMessage:"Action"}),actionName:l({id:`Settings.permissions.auditLogs.${p}`,defaultMessage:V(p)},{model:d?.model})}),e.createElement(W,{actionLabel:l({id:"Settings.permissions.auditLogs.date",defaultMessage:"Date"}),actionName:c}),e.createElement(W,{actionLabel:l({id:"Settings.permissions.auditLogs.user",defaultMessage:"User"}),actionName:i?.displayName||"-"}),e.createElement(W,{actionLabel:l({id:"Settings.permissions.auditLogs.userId",defaultMessage:"User ID"}),actionName:i?.id.toString()||"-"})),e.createElement(ue.V,{value:JSON.stringify(d,null,2),disabled:!0,label:l({id:"Settings.permissions.auditLogs.payload",defaultMessage:"Payload"})}))};U.defaultProps={data:{}},U.propTypes={status:n().oneOf(["idle","loading","error","success"]).isRequired,data:n().shape({action:n().string,date:n().string,payload:n().object,user:n().object}),formattedDate:n().string.isRequired};const ge=U,ee=({handleClose:s,logId:u})=>{const{get:c}=(0,o.kY)(),l=(0,o.lm)(),p=async C=>{const{data:I}=await c(`/admin/audit-logs/${C}`);if(!I)throw new Error("Audit log not found");return I},{data:i,status:d}=(0,j.useQuery)(["audit-log",u],()=>p(u),{onError(){l({type:"warning",message:{id:"notification.error",defaultMessage:"An error occured"}}),s()}}),r=w(),L=i?r(i.date):"";return e.createElement(H.P,{onClose:s,labelledBy:"title"},e.createElement(z.x,null,e.createElement(b.O,{label:L,id:"title"},e.createElement(b.$,null,L))),e.createElement(J.f,null,e.createElement(ge,{status:d,data:i,formattedDate:L})))};ee.propTypes={handleClose:n().func.isRequired,logId:n().string.isRequired};const te=ee;var Y=t(94699),ae=t(29178),se=t(90608);const K=({value:s,options:u,onChange:c})=>{const{formatMessage:l}=(0,y.Z)(),p=l({id:"Settings.permissions.auditLogs.filter.aria-label",defaultMessage:"Search and select an option to filter"});return e.createElement(ae.hQ,{"aria-label":p,value:s,onChange:c},u.map(({label:i,customValue:d})=>e.createElement(se.O,{key:d,value:d},i)))};K.defaultProps={value:null},K.propTypes={value:n().string,options:n().arrayOf(n().shape({label:n().string.isRequired,customValue:n().string.isRequired}).isRequired).isRequired,onChange:n().func.isRequired};const ne=K,X=[{intlLabel:{id:"components.FilterOptions.FILTER_TYPES.$eq",defaultMessage:"is"},value:"$eq"},{intlLabel:{id:"components.FilterOptions.FILTER_TYPES.$ne",defaultMessage:"is not"},value:"$ne"}],he=({formatMessage:s,users:u,canReadUsers:c})=>{const l=Object.keys(k).map(i=>({label:s({id:`Settings.permissions.auditLogs.${i}`,defaultMessage:V(i)},{model:void 0}),customValue:i})),p=[{name:"action",metadatas:{customOperators:X,label:s({id:"Settings.permissions.auditLogs.action",defaultMessage:"Action"}),options:l,customInput:ne},fieldSchema:{type:"enumeration"}},{name:"date",metadatas:{label:s({id:"Settings.permissions.auditLogs.date",defaultMessage:"Date"})},fieldSchema:{type:"datetime"}}];if(c&&u){const i=r=>r.username?r.username:r.firstname&&r.lastname?s({id:"Settings.permissions.auditLogs.user.fullname",defaultMessage:"{firstname} {lastname}"},{firstname:r.firstname,lastname:r.lastname}):r.email,d=u.results.map(r=>({label:i(r),customValue:r.id.toString()}));return[...p,{name:"user",metadatas:{customOperators:X,label:s({id:"Settings.permissions.auditLogs.user",defaultMessage:"User"}),options:d,customInput:ne},fieldSchema:{type:"relation",mainField:{name:"id"}}}]}return p};var Ee=t(16550);const fe=({canReadAuditLogs:s,canReadUsers:u})=>{const{get:c}=(0,o.kY)(),{search:l}=(0,Ee.TH)(),p=(0,o.lm)(),i=async({queryKey:h})=>{const De=h[1],{data:Oe}=await c(`/admin/audit-logs${De}`);return Oe},d=async()=>{const{data:h}=await c("/admin/users");return h},r={keepPreviousData:!0,retry:!1,staleTime:1e3*20,onError:h=>p({type:"warning",message:h.message})},{data:L,isLoading:C,isError:I}=(0,j.useQuery)(["auditLogs",l],i,{...r,enabled:s}),{data:S,isError:_}=(0,j.useQuery)(["auditLogsUsers"],d,{...r,enabled:u,staleTime:2*(1e3*60)}),Pe=I||_;return{auditLogs:L,users:S?.data,isLoading:C,hasError:Pe}},B={...E.Z.settings.auditLogs,readUsers:E.Z.settings.users.read},ye=()=>{const{formatMessage:s}=(0,y.Z)(),{allowedActions:{canRead:u,canReadUsers:c}}=(0,o.ss)(B),[{query:l},p]=(0,o.Kx)(),{auditLogs:i,users:d,isLoading:r,hasError:L}=fe({canReadAuditLogs:u,canReadUsers:c});(0,o.go)();const C=he({formatMessage:s,users:d,canReadUsers:c}),I=s({id:"global.auditLogs",defaultMessage:"Audit Logs"}),S=N.map(_=>({..._,metadatas:{..._.metadatas,label:s(_.metadatas.label)}}));return L?e.createElement(m.A,null,e.createElement(f.D,null,e.createElement(P.x,{paddingTop:8},e.createElement(o.Hn,null)))):e.createElement(T.o,{"aria-busy":r},e.createElement(o.SL,{name:I}),e.createElement(x.T,{title:I,subtitle:s({id:"Settings.permissions.auditLogs.listview.header.subtitle",defaultMessage:"Logs of all the activities that happened in your environment"})}),e.createElement(M.Z,{startActions:e.createElement(Y.Z,{displayedFilters:C})}),e.createElement(f.D,{canRead:u},e.createElement(o.tM,{contentType:"Audit logs",headers:S,rows:i?.results||[],withBulkActions:!0,isLoading:r},e.createElement(de,{headers:S,rows:i?.results||[],onOpenModal:_=>p({id:_})})),e.createElement(D,{pagination:i?.pagination})),l?.id&&e.createElement(te,{handleClose:()=>p({id:null},"remove"),logId:l.id}))},re=()=>e.createElement(o.O4,{permissions:E.Z.settings.auditLogs.main},e.createElement(ye,null))},2407:(F,v,t)=>{t.d(v,{$:()=>x,O:()=>M});var e=t(85893),o=t(16405),E=t(46449),y=t(63237),m=t(11047),f=t(41580),P=t(75515);const T=(0,E.ZP)(m.k)`
  svg {
    height: ${10/16}rem;
    width: ${10/16}rem;
    path {
      fill: ${({theme:g})=>g.colors.neutral500};
    }
  }
  :last-of-type ${f.x} {
    display: none;
  }
  :last-of-type ${P.Z} {
    color: ${({theme:g})=>g.colors.neutral800};
    font-weight: ${({theme:g})=>g.fontWeights.bold};
  }
`,x=({children:g})=>(0,e.jsxs)(T,{inline:!0,as:"li",children:[(0,e.jsx)(P.Z,{variant:"pi",textColor:"neutral600",children:g}),(0,e.jsx)(f.x,{"aria-hidden":!0,paddingLeft:3,paddingRight:3,children:(0,e.jsx)(o.Z,{})})]});x.displayName="Crumb";const M=({children:g,label:n,...O})=>(0,e.jsxs)(m.k,{...O,children:[(0,e.jsx)(y.T,{children:n}),(0,e.jsx)("ol",{"aria-hidden":!0,children:g})]});M.displayName="Breadcrumbs"},29178:(F,v,t)=>{t.d(v,{Wx:()=>ve,XU:()=>ie,hQ:()=>G});var e=t(85893),o=t(67294),E=t(70968),y=t(12645),m=t(61505),f=t(46449),P=t(10892),T=t(2504),x=t(75368),M=t(15585),g=t(77197),n=t(41580),O=t(75515),Z=t(54574),R=t(11047),A=t(19270),$=t(63428),le=t(96404);const G=({children:a,clearLabel:D="clear",creatable:j=!1,createMessage:H=oe=>`Create "${oe}"`,defaultTextValue:z,disabled:b=!1,error:J,hasMoreItems:ce=!1,hint:me,id:ue,label:q,labelAction:W,loading:U=!1,loadingMessage:ge="Loading content...",noOptionsMessage:ee=()=>"No results found",onChange:te,onClear:Y,onCreateOption:ae,onInputChange:se,onLoadMore:K,placeholder:ne="Select or enter a value",required:X=!1,startIcon:pe,textValue:he,value:Ee})=>{const[oe,fe]=o.useState(!1),[B,Me]=(0,P.T)({prop:he,defaultProp:z}),[ye,Le]=o.useState(""),re=o.useRef(null),s=o.useRef(null),u=o.useRef(null),c=(0,T.M)(ue),l=h=>{Y&&!b&&(Me(""),Le(""),Y(h),s.current.focus())},p=h=>{fe(h)},i=h=>{Me(h)},d=h=>{Le(h)},r=h=>{se&&se(h)},L=h=>{te&&te(h)},C=`intersection-${CSS.escape(c)}`,I=h=>{K&&ce&&!U&&K(h)},S=()=>{ae&&B&&ae(B)};(0,x.s)(re,I,{selectorToWatch:`#${C}`,skipWhen:!oe});const _=`${c}-hint`,Pe=`${c}-error`;return(0,e.jsx)(Z.g,{hint:me,error:J,id:c,required:X,children:(0,e.jsxs)(R.k,{direction:"column",alignItems:"stretch",gap:1,children:[(0,e.jsx)(A.Q,{action:W,children:q}),(0,e.jsxs)(m.hQ.Root,{autocomplete:j?"list":"both",onOpenChange:p,onTextValueChange:i,textValue:B,allowCustomValue:!0,disabled:b,required:X,value:Ee,onValueChange:L,filterValue:ye,onFilterValueChange:d,children:[(0,e.jsxs)(w,{$hasError:!!J,children:[(0,e.jsxs)(R.k,{flex:"1",as:"span",gap:3,children:[pe?(0,e.jsx)(n.x,{as:"span","aria-hidden":!0,children:pe}):null,(0,e.jsx)(k,{placeholder:ne,id:c,"aria-invalid":!!J,"aria-labelledby":`${_} ${Pe}`,onChange:r,ref:s})]}),(0,e.jsxs)(R.k,{as:"span",gap:3,children:[B&&Y?(0,e.jsx)(xe,{as:"button",hasRadius:!0,background:"transparent",type:"button",onClick:l,"aria-disabled":b,"aria-label":D,title:D,ref:u,children:(0,e.jsx)(E.Z,{})}):null,(0,e.jsx)(V,{children:(0,e.jsx)(y.Z,{})})]})]}),(0,e.jsx)(m.hQ.Portal,{children:(0,e.jsx)(Q,{sideOffset:4,children:(0,e.jsxs)(de,{ref:re,children:[a,j?(0,e.jsx)(m.hQ.CreateItem,{onPointerUp:S,onClick:S,asChild:!0,children:(0,e.jsx)(N,{children:(0,e.jsx)(O.Z,{children:H(B??"")})})}):null,!j&&!U?(0,e.jsx)(m.hQ.NoValueFound,{asChild:!0,children:(0,e.jsx)(N,{$hasHover:!1,children:(0,e.jsx)(O.Z,{children:ee(B??"")})})}):null,U?(0,e.jsx)(R.k,{justifyContent:"center",alignItems:"center",paddingTop:2,paddingBottom:2,children:(0,e.jsx)(g.a,{small:!0,children:ge})}):null,(0,e.jsx)(n.x,{id:C,width:"100%",height:"1px"})]})})})]}),(0,e.jsx)($.J,{}),(0,e.jsx)(le.c,{})]})})},ie=a=>(0,e.jsx)(G,{...a,creatable:!0}),xe=(0,f.ZP)(n.x)`
  border: none;

  svg {
    height: ${11/16}rem;
    width: ${11/16}rem;
  }

  svg path {
    fill: ${({theme:a})=>a.colors.neutral600};
  }
`,w=(0,f.ZP)(m.hQ.Trigger)`
  position: relative;
  border: 1px solid ${({theme:a,$hasError:D})=>D?a.colors.danger600:a.colors.neutral200};
  padding-right: ${({theme:a})=>a.spaces[3]};
  padding-left: ${({theme:a})=>a.spaces[3]};
  border-radius: ${({theme:a})=>a.borderRadius};
  background: ${({theme:a})=>a.colors.neutral0};
  overflow: hidden;
  display: flex;
  align-items: center;
  justify-content: space-between;
  gap: ${({theme:a})=>a.spaces[4]};

  &[data-disabled] {
    color: ${({theme:a})=>a.colors.neutral600};
    background: ${({theme:a})=>a.colors.neutral150};
    cursor: not-allowed;
  }

  /* Required to ensure the below inputFocusStyles are adhered too */
  &:focus-visible {
    outline: none;
  }

  ${({theme:a,$hasError:D})=>(0,M.k3)()({theme:a,hasError:D})};
`,k=(0,f.ZP)(m.hQ.TextInput)`
  width: 100%;
  font-size: ${14/16}rem;
  color: ${({theme:a})=>a.colors.neutral800};
  min-height: ${40/16}rem;
  border: none;
  background-color: transparent;

  &:focus-visible {
    outline: none;
  }

  &[aria-disabled='true'] {
    cursor: inherit;
  }
`,V=(0,f.ZP)(m.hQ.Icon)`
  & > svg {
    width: ${6/16}rem;

    & > path {
      fill: ${({theme:a})=>a.colors.neutral600};
    }
  }

  &[aria-disabled='true'] {
    cursor: inherit;
  }
`,Q=(0,f.ZP)(m.hQ.Content)`
  background: ${({theme:a})=>a.colors.neutral0};
  box-shadow: ${({theme:a})=>a.shadows.filterShadow};
  border: 1px solid ${({theme:a})=>a.colors.neutral150};
  border-radius: ${({theme:a})=>a.borderRadius};
  width: var(--radix-combobox-trigger-width);
  /* This is from the design-system figma file. */
  max-height: 15rem;
  z-index: ${({theme:a})=>a.zIndices[1]};
`,de=(0,f.ZP)(m.hQ.Viewport)`
  padding: ${({theme:a})=>a.spaces[1]};
`,ve=o.forwardRef(({children:a,value:D,disabled:j,textValue:H,...z},b)=>(0,e.jsx)(m.hQ.ComboboxItem,{asChild:!0,value:D,disabled:j,textValue:H,children:(0,e.jsx)(N,{ref:b,...z,children:(0,e.jsx)(m.hQ.ItemText,{asChild:!0,children:(0,e.jsx)(O.Z,{children:a})})})})),N=f.ZP.div`
  width: 100%;
  border: none;
  text-align: left;
  outline-offset: -3px;
  padding: ${({theme:a})=>a.spaces[2]} ${({theme:a})=>a.spaces[4]};
  background-color: ${({theme:a})=>a.colors.neutral0};
  border-radius: ${({theme:a})=>a.borderRadius};
  user-select: none;

  &[data-selected] {
    background-color: ${({theme:a})=>a.colors.primary100};

    ${O.Z} {
      color: ${({theme:a})=>a.colors.primary600};
      font-weight: bold;
    }
  }

  &:hover,
  &[data-highlighted] {
    outline: none;
    background-color: ${({theme:a,$hasHover:D=!0})=>D?a.colors.primary100:a.colors.neutral0};
  }

  &[data-highlighted] {
    ${O.Z} {
      color: ${({theme:a})=>a.colors.primary600};
      font-weight: bold;
    }
  }
`},90608:(F,v,t)=>{t.d(v,{O:()=>o});var e=t(29178);const o=e.Wx},36989:(F,v,t)=>{t.d(v,{Z:()=>x});var e=t(85893),o=t(67294),E=t(45697),y=t(46449),m=t(11047),f=t(41580);const P=(0,y.ZP)(m.k)`
  & > * + * {
    margin-left: ${({theme:M})=>M.spaces[2]};
  }

  margin-left: ${({pullRight:M})=>M?"auto":void 0};
`,T=(0,y.ZP)(P)`
  flex-shrink: 0;
`,x=({startActions:M,endActions:g})=>M||g?(0,e.jsx)(f.x,{paddingLeft:10,paddingRight:10,children:(0,e.jsx)(f.x,{paddingBottom:4,children:(0,e.jsxs)(m.k,{justifyContent:"space-between",alignItems:"flex-start",children:[M&&(0,e.jsx)(P,{wrap:"wrap",children:M}),g&&(0,e.jsx)(T,{pullRight:!0,children:g})]})})}):null;x.defaultProps={endActions:void 0,startActions:void 0},x.propTypes={endActions:E.node,startActions:E.node}}}]);
